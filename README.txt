Pour compiler le projet :

./gradlew build

Pour lancer le server (port par defaut 6666):

./gradlew server:run
Pour lancer sur un autre port
./gradlew server:run -PappArgs="['8888']"

Pour lancer un client (se connected par defaut sur le localhost, port 6666)
./gradlew client:run

./gradlew client:run -PappArgs="['ip-server', '8888']"