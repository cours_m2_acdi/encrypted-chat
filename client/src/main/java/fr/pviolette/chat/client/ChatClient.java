package fr.pviolette.chat.client;

import fr.pviolette.chat.client.view.ChatGUI;
import fr.pviolette.chat.client.view.ConsoleView;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ChatClient {

    private static final Logger logger = LogManager.getLogger();

    private static boolean stopped = false;

    private static Options buildOptions() {
        Options options = new Options();

        Option ipOption = new Option("i", "ip", true, "Server ip adress");
        ipOption.setRequired(false);
        options.addOption(ipOption);

        Option portOption = new Option("p", "port", true, "Port");
        portOption.setRequired(false);
        options.addOption(portOption);

        Option headlessOption = new Option("h", "headless", false, "Run in console");
        options.addOption(headlessOption);

        Option aliasOption = new Option("a", "alias", true, "Alias");
        options.addOption(aliasOption);

        return options;
    }

    public static void main(String[] args){

        Options options = ChatClient.buildOptions();

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("h")) {
                if (!cmd.hasOption("p")) {
                    System.err.println("Port options --port is required in headless mode");
                    System.exit(1);
                }
                if (!cmd.hasOption("i")) {
                    System.err.println("Argument --ip is required in headless mode");
                    System.exit(1);
                }

                InetAddress address = InetAddress.getByName(cmd.getOptionValue("ip"));
                int port = Integer.parseInt(cmd.getOptionValue("port"));

                ConsoleView consoleView = new ConsoleView();
                consoleView.run(address, port, cmd.getOptionValue("alias"));

            } else {

                final String serverAdress = cmd.getOptionValue("ip");
                final String alias = cmd.getOptionValue("alias");
                final Integer port = Integer.parseInt(cmd.getOptionValue("port"));


                SwingUtilities.invokeLater(() -> {
                    try {
                        UIManager.setLookAndFeel(
                                "com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ChatGUI chatGUI = new ChatGUI();

                    chatGUI.preDisplay(serverAdress, port, alias);
                });
            }
        } catch (ParseException e) {
            System.err.println("Parsing failed.  Reason: " + e.getMessage());
            System.exit(1);
        } catch (UnknownHostException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

    }

}
