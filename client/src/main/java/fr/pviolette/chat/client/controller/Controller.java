package fr.pviolette.chat.client.controller;

import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.Utilities;
import fr.pviolette.chat.commons.rsa.RsaDecrypter;
import fr.pviolette.chat.commons.rsa.RsaEncrypter;
import fr.pviolette.chat.commons.rsa.key.RsaKeyGenerator;
import fr.pviolette.chat.commons.rsa.key.RsaKeySet;
import fr.pviolette.chat.commons.rsa.key.RsaPublicKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;

public class Controller {

    private final static Logger logger = LogManager.getLogger();

    private Collection<Message> messageCollection;

    private Socket socket;

    private Receiver receiver;

    private MessageHandler messageHandler;

    private Collection<MessagesListener> messagesListeners;

    private RsaKeySet keySet;

    private RsaPublicKey serverPublicKey;

    private RsaEncrypter rsaEncrypter;
    private RsaDecrypter decrypter;

    private Optional<String> startingAlias;

    public Controller() {
        this.messageCollection = new PriorityQueue<>(
                Comparator.comparing(Message::getTime)
                        .thenComparing(Comparator.comparing(Message::getId)) //Two message at the exact same instant are unlikely to happen, but just in case
        );

        this.messageHandler = new MessageHandler(this);

        this.messagesListeners = new ArrayList<>();

        RsaKeyGenerator rsaKeyGenerator = new RsaKeyGenerator(128);


        this.keySet = rsaKeyGenerator.generateKeys();
        BigInteger bi = this.keySet.getPublicKey().getE().modPow(this.keySet.getPublicKey().getE(), this.keySet.getPublicKey().getN());
        //On aura donc des blocs toujours inferieur à n
        int blocsize = bi.toByteArray().length - 10;
        this.decrypter = new RsaDecrypter(this.keySet.getPrivateKey(), blocsize);
    }

    public void connectToServer(InetAddress address, int port) throws IOException{
        this.connectToServer(address, port, null);
    }

    public void connectToServer(InetAddress address, int port, String alias) throws IOException{

        this.startingAlias = Optional.ofNullable(alias);

        if(this.receiver != null && this.receiver.isAlive()){
            this.receiver.interrupt();
        }

        this.socket = new Socket(address, port);

        if(socket.isConnected()){
            this.receiver = new Receiver(this);

            receiver.start();
        }else{
            logger.error("Unable to connect to server " + address + ":" + port);
        }
    }

    void addMessage(Message message){

        messageCollection.add(message);
        notifyOfNewMessage(message);
    }

    public Socket getSocket() {
        return socket;
    }

    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    public void sendMessage(Message message) throws IOException {
        String jsonString = message.toJsonString();

        //Encrypt message
        byte[] encrypted = this.rsaEncrypter.encrypt(jsonString.getBytes());
        logger.debug("Ecrypting with key " + this.rsaEncrypter.getPublicKey());
        try{
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(encrypted);
            outputStream.flush();
            logger.debug("Send message " + jsonString);
            logger.debug("Encrypted : " + Utilities.bytesToString(encrypted));
        }catch (IOException e){
            logger.error("Unable to send message", e);
            this.socket.close();
            this.stop();
        }
    }

    private void notifyOfNewMessage(Message message){
        logger.debug("Starting notification of new messages");
        for(MessagesListener listener : this.messagesListeners){
            logger.debug("Notifying " + listener);
            listener.notifyNewMessage(message);
        }
    }

    public void registerMessagesListener(MessagesListener listener){
        this.messagesListeners.add(listener);
    }

    public void removeMessagesListener(MessagesListener listener){
        this.messagesListeners.remove(listener);
    }

    public void setServerPublicKey(RsaPublicKey serverPublicKey) {
        this.serverPublicKey = serverPublicKey;
        logger.debug("Server public key : " + serverPublicKey);
        BigInteger bi = this.keySet.getPublicKey().getE().modPow(this.keySet.getPublicKey().getE(), this.keySet.getPublicKey().getN());
        //On aura donc des blocs toujours inferieur à n
        int blocsize = bi.toByteArray().length - 10;

        this.rsaEncrypter = new RsaEncrypter(this.serverPublicKey, blocsize);
    }

    public boolean isKeyExchangeDone(){
        return this.serverPublicKey != null;
    }

    public RsaDecrypter getDecrypter() {
        return decrypter;
    }

    public RsaPublicKey getPublicKey(){
        return this.keySet.getPublicKey();
    }

    public Optional<String> getStartingAlias() {
        return startingAlias;
    }

    public void stop() {
        for (MessagesListener listener : messagesListeners) {
            listener.notifyStop();
        }
    }
}
