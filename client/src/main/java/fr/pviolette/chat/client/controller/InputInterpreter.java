package fr.pviolette.chat.client.controller;

import fr.pviolette.chat.commons.Command;
import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.User;

import java.lang.reflect.Array;
import java.time.Instant;
import java.util.Arrays;

public class InputInterpreter {

    private Message buildPrivateMessage(String[] split, String input){

        String dest = split[1];

        String content = input.replaceAll(split[0] + "\\s+" + split[1], ""); //will not work with some char

        Message message = new Message(Instant.now(), content, Message.Type.PRIVATE);
        User recipient = new User();
        recipient.setAlias(dest);
        message.setRecipient(recipient);

        return message;
    }

    private Message buildAliasChangerMessage(String split[]){
        String newAlias = split[1];

        Command command = new Command();
        command.setType(Command.Type.CHANGE_ALIAS);
        command.setArgs(new String[]{newAlias});

        Message message = new Message(Instant.now(), command.toJSONString(), Message.Type.COMMAND_ALL);
        return message;
    }

    public Message interpret(String input){
        if(input.startsWith("/")){

            String[] split = input.split("\\s+");

            String cmd = split[0].substring(1);

            switch (split[0]){
                case "/w": //Private message
                        return buildPrivateMessage(split, input);
                case "/alias":
                        return buildAliasChangerMessage(split);
                case "/list":
                        return buildListMessage();
                case "/stop":
                        return buildStopMessage();
                    default:
                        return new Message(Instant.now(), input, Message.Type.ALL);
            }

        }else{
            return new Message(Instant.now(), input, Message.Type.ALL);
        }
    }

    private Message buildListMessage() {
        Command command = new Command();
        command.setType(Command.Type.LIST_USER);

        return new Message(Instant.now(), command.toJSONString(), Message.Type.COMMAND_ALL);
    }

    private Message buildStopMessage(){
        Command command = new Command();
        command.setType(Command.Type.STOP);

        return new Message(Instant.now(), command.toJSONString(), Message.Type.COMMAND_ALL);
    }
}
