package fr.pviolette.chat.client.controller;

import fr.pviolette.chat.commons.Command;
import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.rsa.key.RsaPublicKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.math.BigInteger;
import java.time.Instant;


public class MessageHandler {

    private static final Logger logger = LogManager.getLogger();


    private Controller controller;

    public MessageHandler(Controller controller) {
        this.controller = controller;
    }

    void handleMessage(Message message) throws IOException {

        if(message.getType() == Message.Type.COMMANDE_PRIVATE){
            logger.debug("Received command private message : " + message.toJsonString());
            Command command = Command.fromJSONString(message.getContent());
            switch (command.getType()){
                case PUBLIC_KEY:
                    this.handlePublicKeyCommand(command);
                    return;
            }
        }

        this.controller.addMessage(message);
    }

    private void handlePublicKeyCommand(Command command) throws IOException {

        String[] args = command.getArgs();

        if(args == null || args.length != 2){
//            logger.error("Not a valid PublicKey command" + command.toString());
            return;
        }

        try{
            BigInteger n = new BigInteger(args[0]);
            BigInteger e = new BigInteger(args[1]);

            RsaPublicKey publicKey = new RsaPublicKey(n, e);

            logger.debug("Public key : " + publicKey.toString());

            this.controller.setServerPublicKey(publicKey);


            Command publicKeyCmd = new Command();
            publicKeyCmd.setType(Command.Type.PUBLIC_KEY);

            String[] cmdArgs;

            if(this.controller.getStartingAlias().isPresent()){
                cmdArgs = new String[]{this.controller.getPublicKey().getN().toString(), this.controller.getPublicKey().getE().toString(), this.controller.getStartingAlias().get()};
            }else{
                cmdArgs = new String[]{this.controller.getPublicKey().getN().toString(), this.controller.getPublicKey().getE().toString()};
            }

            publicKeyCmd.setArgs(cmdArgs);
            Message ourPublicKey = new Message(Instant.now(), publicKeyCmd.toJSONString(), Message.Type.COMMANDE_PRIVATE);

            this.controller.sendMessage(ourPublicKey);
        } catch (NumberFormatException e){
//            logger.error("Not a valid public key command : " + command.toString());
        }
    }

}
