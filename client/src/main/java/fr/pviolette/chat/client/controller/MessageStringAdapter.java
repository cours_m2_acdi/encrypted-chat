package fr.pviolette.chat.client.controller;

import fr.pviolette.chat.commons.Message;

public interface MessageStringAdapter {

    String adaptMessage(Message message);

}
