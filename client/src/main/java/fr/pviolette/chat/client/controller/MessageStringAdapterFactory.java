package fr.pviolette.chat.client.controller;

import com.google.common.collect.Maps;
import fr.pviolette.chat.client.messageadapter.console.*;
import fr.pviolette.chat.commons.Message;

import java.util.Map;

public class MessageStringAdapterFactory {

    public static MessageStringAdapter getConsoleAdapter(){
        final Map<Message.Type, MessageStringAdapter> adapterMap = Maps.newHashMap();

        final CommandMessageAdapter commandMessageAdapter = new CommandMessageAdapter();

        adapterMap.put(Message.Type.COMMAND_ALL, commandMessageAdapter);
        adapterMap.put(Message.Type.COMMANDE_PRIVATE, commandMessageAdapter);

        final ServerMessageAdapter serverMessageAdapter = new ServerMessageAdapter();

        adapterMap.put(Message.Type.SERVER_ALL, serverMessageAdapter);
        adapterMap.put(Message.Type.SERVER_PRIVATE, serverMessageAdapter);

        final PrivateMessageAdapter privateMessageAdapter = new PrivateMessageAdapter();

        adapterMap.put(Message.Type.PRIVATE, privateMessageAdapter);

        final PublicMessageAdapter publicMessageAdapter = new PublicMessageAdapter();

        adapterMap.put(Message.Type.ALL, publicMessageAdapter);

        final UserDisconnectionMessageAdapter userDisconnectionMessageAdapter = new UserDisconnectionMessageAdapter();

        adapterMap.put(Message.Type.USER_DISCONNECTION, userDisconnectionMessageAdapter);

        final UserConnectionMessageAdapter userConnectionMessageAdapter = new UserConnectionMessageAdapter();

        adapterMap.put(Message.Type.USER_CONNECTION, userConnectionMessageAdapter);

        return new MessageStringAdapterImpl(adapterMap);
    }

    public static MessageStringAdapter getGuiAdapter(){
        final Map<Message.Type, MessageStringAdapter> adapterMap = Maps.newHashMap();

        final fr.pviolette.chat.client.messageadapter.gui.CommandMessageAdapter commandMessageAdapter = new fr.pviolette.chat.client.messageadapter.gui.CommandMessageAdapter();
        adapterMap.put(Message.Type.COMMAND_ALL, commandMessageAdapter);
        adapterMap.put(Message.Type.COMMANDE_PRIVATE, commandMessageAdapter);

        final fr.pviolette.chat.client.messageadapter.gui.ServerMessageAdapter serverMessageAdapter = new fr.pviolette.chat.client.messageadapter.gui.ServerMessageAdapter();

        adapterMap.put(Message.Type.SERVER_ALL, serverMessageAdapter);
        adapterMap.put(Message.Type.SERVER_PRIVATE, serverMessageAdapter);

        final fr.pviolette.chat.client.messageadapter.gui.PrivateMessageAdapter privateMessageAdapter = new fr.pviolette.chat.client.messageadapter.gui.PrivateMessageAdapter();

        adapterMap.put(Message.Type.PRIVATE, privateMessageAdapter);

        final fr.pviolette.chat.client.messageadapter.gui.PublicMessageAdapter publicMessageAdapter = new fr.pviolette.chat.client.messageadapter.gui.PublicMessageAdapter();

        adapterMap.put(Message.Type.ALL, publicMessageAdapter);

        final fr.pviolette.chat.client.messageadapter.gui.UserDisconnectionMessageAdapter userDisconnectionMessageAdapter = new fr.pviolette.chat.client.messageadapter.gui.UserDisconnectionMessageAdapter();

        adapterMap.put(Message.Type.USER_DISCONNECTION, userDisconnectionMessageAdapter);

        final fr.pviolette.chat.client.messageadapter.gui.UserConnectionMessageAdapter userConnectionMessageAdapter = new fr.pviolette.chat.client.messageadapter.gui.UserConnectionMessageAdapter();

        adapterMap.put(Message.Type.USER_CONNECTION, userConnectionMessageAdapter);

        return new MessageStringAdapterImpl(adapterMap);
    }
}
