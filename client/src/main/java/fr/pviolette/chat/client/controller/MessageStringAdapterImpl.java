package fr.pviolette.chat.client.controller;

import fr.pviolette.chat.commons.Message;

import java.util.Map;


public class MessageStringAdapterImpl implements MessageStringAdapter {

    private final Map<Message.Type, MessageStringAdapter> adapters;

    public MessageStringAdapterImpl(Map<Message.Type, MessageStringAdapter> adapters) {
        this.adapters = adapters;
    }

    @Override
    public String adaptMessage(Message message) {
        MessageStringAdapter adapter = adapters.get(message.getType());
        if (adapter == null) {
            throw new UnsupportedOperationException("No adapter set for type " + message.getType());
        }

        return adapter.adaptMessage(message);
    }
}


