package fr.pviolette.chat.client.controller;

import fr.pviolette.chat.commons.Message;

public interface MessagesListener {

    void notifyNewMessage(Message message);

    void notifyStop();
}
