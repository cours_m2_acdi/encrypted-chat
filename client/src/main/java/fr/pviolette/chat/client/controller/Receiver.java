package fr.pviolette.chat.client.controller;

import fr.pviolette.chat.commons.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Receiver extends Thread{

    private static Logger logger = LogManager.getLogger();

    private BufferedInputStream inputStream;

    private int bufferSize = 256;

    private Controller controller;

    public Receiver(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void run() {
        try {
            while (! this.controller.getSocket().isClosed()) {
                inputStream = new BufferedInputStream(this.controller.getSocket().getInputStream());

                byte[] messageBytes = read();
                Message message;
                String messageJson;
                if(this.controller.isKeyExchangeDone()){
                    byte[] decryptedBytes = this.controller.getDecrypter().decrypt(messageBytes);
                    messageJson = new String(decryptedBytes);

                }else{
                    messageJson = new String(messageBytes);
                }

                message = Message.fromJsonString(messageJson);

                logger.info("Received " + message.toString());

                this.controller.getMessageHandler().handleMessage(message);

           }
        } catch (IOException e) {
            e.printStackTrace();
            this.controller.stop();
        }
    }

    @Override
    public void interrupt() {
        try{
            inputStream.close();
        }catch(IOException e){
        }
        super.interrupt();
    }

    private byte[] read() throws IOException{

        byte[] bytes = new byte[bufferSize];
        int lenght = inputStream.read(bytes);

        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byteArrayOutputStream.write(bytes, 0, lenght);
        while(inputStream.available() != 0){
            lenght = inputStream.read(bytes);
             byteArrayOutputStream.write(bytes, 0, lenght);
        }

        return byteArrayOutputStream.toByteArray();
    }
}
