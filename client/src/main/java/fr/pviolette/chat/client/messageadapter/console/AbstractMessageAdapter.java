package fr.pviolette.chat.client.messageadapter.console;

import fr.pviolette.chat.client.controller.MessageStringAdapter;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public abstract class AbstractMessageAdapter implements MessageStringAdapter{

    DateTimeFormatter dateTimeFormatter;

    public AbstractMessageAdapter() {

        this.dateTimeFormatter =
                DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                        .withLocale(Locale.getDefault())
                        .withZone( ZoneId.systemDefault() );

    }

    public AbstractMessageAdapter(DateTimeFormatter dateTimeFormatter){
        this.dateTimeFormatter = dateTimeFormatter;
    }

    protected String getFormatedInstantString(Instant instant){
        return this.dateTimeFormatter.format(instant);
    }
}
