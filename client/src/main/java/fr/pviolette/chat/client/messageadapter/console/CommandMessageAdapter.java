package fr.pviolette.chat.client.messageadapter.console;

import fr.pviolette.chat.client.AnsiColor;
import fr.pviolette.chat.commons.Command;
import fr.pviolette.chat.commons.Message;

import java.time.format.DateTimeFormatter;

public class CommandMessageAdapter extends AbstractMessageAdapter{

    public CommandMessageAdapter() {
    }

    public CommandMessageAdapter(DateTimeFormatter dateTimeFormatter) {
        super(dateTimeFormatter);
    }

    @Override
    public String adaptMessage(Message message) {
        StringBuilder stringBuilder = new StringBuilder(
                this.getFormatedInstantString(message.getTime())
        ).append("\t");
        if(message.getType() == Message.Type.COMMAND_ALL){
            stringBuilder.append(AnsiColor.ANSI_GREEN);
        }else{
            stringBuilder.append(AnsiColor.ANSI_CYAN);
        }
        Command command = Command.fromJSONString(message.getContent());

        switch (command.getType()){
            case CHANGE_ALIAS:
                String oldAlias = command.getArgs()[1];
                String newAlias = command.getArgs()[2];
                stringBuilder.append(oldAlias).append(" is now know as ").append(newAlias);
                break;
                //No other case for now
        }
        stringBuilder.append(AnsiColor.ANSI_RESET);

        return stringBuilder.toString();
    }
}
