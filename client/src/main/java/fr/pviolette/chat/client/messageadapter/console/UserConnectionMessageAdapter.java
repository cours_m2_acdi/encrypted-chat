package fr.pviolette.chat.client.messageadapter.console;

import fr.pviolette.chat.client.AnsiColor;
import fr.pviolette.chat.commons.Message;

import java.time.format.DateTimeFormatter;

public class UserConnectionMessageAdapter extends AbstractMessageAdapter{

    public UserConnectionMessageAdapter() {
    }

    public UserConnectionMessageAdapter(DateTimeFormatter dateTimeFormatter) {
        super(dateTimeFormatter);
    }

    @Override
    public String adaptMessage(Message message) {
        return this.getFormatedInstantString(message.getTime()) + AnsiColor.ANSI_YELLOW + "\tUser " + message.getSender().getAlias() + " is now connected" + AnsiColor.ANSI_RESET;
    }
}
