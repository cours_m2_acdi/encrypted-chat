package fr.pviolette.chat.client.messageadapter.console;

import fr.pviolette.chat.client.AnsiColor;
import fr.pviolette.chat.commons.Message;

import java.time.format.DateTimeFormatter;

public class UserDisconnectionMessageAdapter extends AbstractMessageAdapter {

    public UserDisconnectionMessageAdapter() {
    }

    public UserDisconnectionMessageAdapter(DateTimeFormatter dateTimeFormatter) {
        super(dateTimeFormatter);
    }

    @Override
    public String adaptMessage(Message message) {
        return this.getFormatedInstantString(message.getTime()) + AnsiColor.ANSI_YELLOW
                + "User " + message.getSender().getAlias() + " has disconnected" + AnsiColor.ANSI_RESET;
    }
}
