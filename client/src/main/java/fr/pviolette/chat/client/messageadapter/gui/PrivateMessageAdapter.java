package fr.pviolette.chat.client.messageadapter.gui;

import fr.pviolette.chat.client.AnsiColor;
import fr.pviolette.chat.commons.Message;

import java.time.format.DateTimeFormatter;

public class PrivateMessageAdapter extends AbstractMessageAdapter {

    public PrivateMessageAdapter() {
    }

    public PrivateMessageAdapter(DateTimeFormatter dateTimeFormatter) {
        super(dateTimeFormatter);
    }

    @Override
    public String adaptMessage(Message message) {
        return this.getFormatedInstantString(message.getTime()) + " "
//                + AnsiColor.ANSI_BLUE
                + "From " + message.getSender().getAlias() + " "
                + message.getContent() + "\n";
    }
}
