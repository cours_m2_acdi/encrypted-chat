package fr.pviolette.chat.client.messageadapter.gui;

import fr.pviolette.chat.commons.Message;

import java.time.format.DateTimeFormatter;

public class PublicMessageAdapter extends AbstractMessageAdapter {

    public PublicMessageAdapter(DateTimeFormatter dateTimeFormatter) {
        super(dateTimeFormatter);
    }

    public PublicMessageAdapter() {

    }

    @Override
    public String adaptMessage(Message message) {
        if(message.getSender() == null){
            System.out.println("null sender");
        }
        return getFormatedInstantString(message.getTime()) + " " +
                message.getSender().getAlias() + " : " + message.getContent() + "\n" ;
    }
}
