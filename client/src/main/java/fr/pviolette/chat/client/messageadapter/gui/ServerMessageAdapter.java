package fr.pviolette.chat.client.messageadapter.gui;

import fr.pviolette.chat.client.AnsiColor;
import fr.pviolette.chat.commons.Message;

import java.time.format.DateTimeFormatter;

public class ServerMessageAdapter extends AbstractMessageAdapter {
    public ServerMessageAdapter() {
    }

    public ServerMessageAdapter(DateTimeFormatter dateTimeFormatter) {
        super(dateTimeFormatter);
    }

    @Override
    public String adaptMessage(Message message) {
        return this.getFormatedInstantString(message.getTime()) + " "
//                + AnsiColor.ANSI_RED
                + "SERVER - " + message.getContent() + "\n"
//                + AnsiColor.ANSI_RESET
                ;
    }
}
