package fr.pviolette.chat.client.messageadapter.gui;

import fr.pviolette.chat.commons.Message;

import java.time.format.DateTimeFormatter;

public class UserConnectionMessageAdapter extends AbstractMessageAdapter {

    public UserConnectionMessageAdapter() {
    }

    public UserConnectionMessageAdapter(DateTimeFormatter dateTimeFormatter) {
        super(dateTimeFormatter);
    }

    @Override
    public String adaptMessage(Message message) {
        return this.getFormatedInstantString(message.getTime())
                + " User "
                + message.getSender().getAlias()
                + " is now connected\n";
    }
}
