package fr.pviolette.chat.client.view;

import fr.pviolette.chat.client.controller.*;
import fr.pviolette.chat.commons.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

public class ChatFrame extends JFrame implements MessagesListener, KeyListener {

    private final static Logger logger = LogManager.getLogger();

    private JTextField messageBox;
    private JTextArea chatBox;
    private JButton sendMessage;
    private InputInterpreter inputInterpreter;
    private Controller controller;

    private MessageStringAdapter messageStringAdapter = MessageStringAdapterFactory.getGuiAdapter();

    public ChatFrame(Controller controller) throws HeadlessException {
        this.inputInterpreter = new InputInterpreter();
        this.controller = controller;

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        JPanel southPanel = new JPanel();
        southPanel.setBackground(Color.BLUE);
        southPanel.setLayout(new GridBagLayout());

        messageBox = new JTextField(30);
        messageBox.requestFocusInWindow();
        messageBox.addKeyListener(this);

        sendMessage = new JButton("Send Message");

        chatBox = new JTextArea();
        chatBox.setEditable(false);
        chatBox.setFont(new Font("Serif", Font.PLAIN, 15));
        chatBox.setLineWrap(true);

        mainPanel.add(new JScrollPane(chatBox), BorderLayout.CENTER);

        GridBagConstraints left = new GridBagConstraints();
        left.anchor = GridBagConstraints.LINE_START;
        left.fill = GridBagConstraints.HORIZONTAL;
        left.weightx = 512.0D;
        left.weighty = 1.0D;

        GridBagConstraints right = new GridBagConstraints();
        right.insets = new Insets(0, 10, 0, 0);
        right.anchor = GridBagConstraints.LINE_END;
        right.fill = GridBagConstraints.NONE;
        right.weightx = 1.0D;
        right.weighty = 1.0D;

        southPanel.add(messageBox, left);
        southPanel.add(sendMessage, right);

        mainPanel.add(BorderLayout.SOUTH, southPanel);

        this.add(mainPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(470, 300);
        this.setVisible(true);

        sendMessage.addActionListener(actionEvent -> sendMessage());

        controller.registerMessagesListener(this);
    }

    private void sendMessage() {
        if (messageBox.getText().equals(".clear")) {
            chatBox.setText("Cleared all messages\n");
            messageBox.setText("");
        } else if (!messageBox.getText().isEmpty()) {

            String messageStr = messageBox.getText();

            inputInterpreter = new InputInterpreter();

            Message message = inputInterpreter.interpret(messageStr);
            logger.debug("Send message [" + messageStr + "] -> " + message.toJsonString());
            try {
                controller.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }

            messageBox.setText("");
        }

    }

    @Override
    public void notifyNewMessage(Message message) {
        this.chatBox.append(this.messageStringAdapter.adaptMessage(message));
    }

    @Override
    public void notifyStop() {

    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
            this.sendMessage();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
