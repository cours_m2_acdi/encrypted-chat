package fr.pviolette.chat.client.view;

import fr.pviolette.chat.client.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;


public class ChatGUI {

    private final static Logger logger = LogManager.getLogger();

    private JFrame newFrame;
    private JFrame preFrame;

    public void preDisplay(String defaultServerAdress, Integer defaultPort, String defaultAlias) {
        this.preFrame = new ServerChooserFrame(this, defaultServerAdress, defaultPort, defaultAlias);
        this.preFrame.setVisible(true);
    }

    public void display(Controller controller) {
        this.newFrame = new ChatFrame(controller);
        this.newFrame.setVisible(true);
    }

}
