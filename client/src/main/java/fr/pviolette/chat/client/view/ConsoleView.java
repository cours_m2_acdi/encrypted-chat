package fr.pviolette.chat.client.view;

import fr.pviolette.chat.client.controller.*;
import fr.pviolette.chat.commons.Message;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Scanner;

public class ConsoleView implements MessagesListener {

    private Controller controller;

    private MessageStringAdapter messageStringAdapter;
    private InputInterpreter inputInterpreter;
    private boolean stopped = false;

    public ConsoleView() {
        this.messageStringAdapter = MessageStringAdapterFactory.getConsoleAdapter();
        this.inputInterpreter = new InputInterpreter();
    }

    public void run(InetAddress serverAdress, int port, String username) {
        Scanner scanner = new Scanner(System.in);
        try {

            this.controller = new Controller();
            this.controller.registerMessagesListener(this);

            controller.connectToServer(serverAdress, port, username);

            while (!this.stopped) {
                String input = scanner.nextLine();

                this.sendMessage(input);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String input) throws IOException {

        Message message = inputInterpreter.interpret(input);
        this.controller.sendMessage(message);
    }

    public void printMessage(Message message){
        System.out.println(messageStringAdapter.adaptMessage(message));
    }

    @Override
    public void notifyNewMessage(Message message) {
        printMessage(message);
    }

    public void stop() {
        this.stopped = true;
    }

    @Override
    public void notifyStop() {
        this.stop();
    }
}
