package fr.pviolette.chat.client.view;

import fr.pviolette.chat.client.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.InetAddress;

public class ServerChooserFrame extends JFrame implements KeyListener {

    private JTextField usernameChooser;
    private JTextField portChooser;
    private JTextField serverChooser;
    private JButton buttonOk;
    private ChatGUI chatGUI;
    private JPanel panel;

    public ServerChooserFrame(ChatGUI chatGUI, String defaultServerAdress, Integer defaultPort, String defaultAlias) throws HeadlessException {
        this.chatGUI = chatGUI;

        usernameChooser = new JTextField(150);
        portChooser = new JTextField(150);
        serverChooser = new JTextField(150);

        if (defaultAlias != null) {
            usernameChooser.setText(defaultAlias);
        }
        if (defaultPort != null) {
            this.portChooser.setText(String.valueOf(defaultPort));
        }
        if (defaultServerAdress != null) {
            this.serverChooser.setText(defaultServerAdress);
        }

        usernameChooser.addKeyListener(this);
        portChooser.addKeyListener(this);
        serverChooser.addKeyListener(this);

        JLabel chooseUsernameLabel = new JLabel("Pick a username:");
        JLabel choosePortLabel = new JLabel("Enter port number:");
        JLabel chooseServerLabel = new JLabel("Enter Chat Server : ");

        this.panel = new JPanel(new GridBagLayout());
        this.buttonOk = new JButton("Ok");
        GridBagConstraints preRight = new GridBagConstraints();
        preRight.insets = new Insets(0, 0, 0, 10);
        preRight.anchor = GridBagConstraints.EAST;
        GridBagConstraints preLeft = new GridBagConstraints();
        preLeft.anchor = GridBagConstraints.WEST;
        preLeft.insets = new Insets(0, 10, 0, 10);
        preRight.fill = GridBagConstraints.HORIZONTAL;
        preRight.weightx = 3;
        preRight.gridwidth = GridBagConstraints.REMAINDER;

        this.panel.add(chooseUsernameLabel, preLeft);
        this.panel.add(usernameChooser, preRight);
        this.panel.add(chooseServerLabel, preLeft);
        this.panel.add(serverChooser, preRight);
        this.panel.add(choosePortLabel, preLeft);
        this.panel.add(portChooser, preRight);
        this.add(BorderLayout.CENTER, panel);
        this.add(BorderLayout.SOUTH, buttonOk);
        this.setSize(470, 300);

        ServerChooserFrame this2 = this;
        this.buttonOk.addActionListener(actionEvent -> this2.submit());
    }

    private void submit() {
        try {

            String username = (usernameChooser).getText();
            String server = (serverChooser).getText();
            int port = Integer.parseInt((portChooser).getText());

            Controller controller = new Controller();
            controller.connectToServer(InetAddress.getByName(server), port, username);
            this.panel.setVisible(false);
            this.chatGUI.display(controller);

        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage());
        }

    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
            this.submit();
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_TAB) {
            this.getNextInput(keyEvent.getComponent()).requestFocusInWindow();
        }
    }

    public JComponent getNextInput(Component component) {
        if (component == this.usernameChooser) {
            return this.serverChooser;
        } else if (component == this.serverChooser) {
            return this.portChooser;
        } else {
            return this.portChooser;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
