package fr.pviolette.chat.commons;

import com.google.gson.Gson;

import java.util.Arrays;

public class Command {

    private static Gson gson = new Gson();

    public static final int UNDEFINED_ARITY = -1;

    public enum Type{
        CHANGE_ALIAS("alias", 1),
        LIST_USER("list", 0),
        PUBLIC_KEY("key", 3),
        STOP("stop", 0);

        private String command;
        private int expectedArgs;

        Type(String command, int expectedArgs) {
            this.command = command;
            this.expectedArgs = expectedArgs;
        }

        public String getCommand() {
            return command;
        }

        public int getExpectedArgs() {
            return expectedArgs;
        }

        public static Type fromString(String str){
            for(Type type : values()){
                if(type.getCommand().equals(str)){
                    return type;
                }
            }
            return null;
        }
    }

    private Type type;
    private String[] args;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public String toJSONString(){
        return gson.toJson(this);
    }

    public static Command fromJSONString(String jsonString){
        return gson.fromJson(jsonString, Command.class);

    }

    @Override
    public String toString() {
        return "Command{" +
                "type=" + type +
                ", args=" + Arrays.toString(args) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Command command = (Command) o;

        if (type != command.type) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(args, command.args);
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(args);
        return result;
    }
}
