package fr.pviolette.chat.commons;

import com.google.gson.Gson;

import java.time.Instant;

public class Message {

    public enum Type{
        SERVER_ALL(true),
        SERVER_PRIVATE(false),
        USER_CONNECTION(true),
        USER_DISCONNECTION(true),
        PRIVATE(false),
        ALL(true),
        COMMANDE_PRIVATE(false),
        COMMAND_ALL(true);

        private boolean sendToAll;

        Type(boolean sendToAll) {
            this.sendToAll = sendToAll;
        }

        public boolean isSendToAll(){
            return this.sendToAll;
        }
    }

    private static Gson gson = new Gson();

    private Instant time;
    private String content;
    private User sender;
    private int id;

    private User recipient;

    private Type type;

    public Message() {
        this.time = Instant.now();
        this.content = "";
        this.id = -1;
    }

    public Message(Instant time, String content, Type type) {
        this.time = time;
        this.content = content;
        this.type = type;
    }

    public Instant getTime() {
        return time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String toJsonString(){
        return gson.toJson(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSender(User sender) {
        if(this.sender != sender){
            this.sender = sender;
        }
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public Type getType() {
        return type;
    }

    public User getSender() {
        return sender;
    }

    public static Message fromJsonString(String json){
        return gson.fromJson(json, Message.class);
    }

    @Override
    public String toString() {
        return "Message{" +
                "time=" + time +
                ", content='" + content + '\'' +
                ", sender=" + sender +
                ", id=" + id +
                ", recipient=" + recipient +
                ", type=" + type +
                '}';
    }
}
