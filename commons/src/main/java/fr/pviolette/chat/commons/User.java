package fr.pviolette.chat.commons;

import java.lang.reflect.Array;
import java.util.*;

public class User {

    private int id;
    private String alias;

    private int score;
    private int currentTurnScore;

    private int[] currentDices;
    private boolean playing =false;

    private int bestScore = 0;

    public User() {
        this.id = -1;
        initialize();
    }

    public User(int id) {
        this.id = id;
        initialize();
    }

    private void initialize(){
        this.alias = "User_" + this.id;
        this.currentDices = new int[0];
        this.score = 0;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", alias='" + alias + '\'' +
                ", score=" + score +
                ", playing=" + playing +
                ", currentDices=" + Arrays.toString(currentDices) +
                '}';
    }

    public int getCurrentTurnScore() {
        return currentTurnScore;
    }

    public void setCurrentTurnScore(int currentTurnScore) {
        this.currentTurnScore = currentTurnScore;
    }

    public void setCurrentDices(int[] currentDices) {
        this.currentDices = currentDices;
    }

    public int getBestScore() {
        return bestScore;
    }
 }
