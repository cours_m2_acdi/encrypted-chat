package fr.pviolette.chat.commons;

public class Utilities {
    public static String bytesToString(byte[] encrypted)
    {

        String test = "";

        for (byte b : encrypted)

        {

            test += Byte.toString(b);

        }

        return test;

    }

    /**
     * Convertit un entier en 4 bytes
     */
    public static byte[] intToBytes(int i){
        byte res[] = new byte[4];
        res[0] = (byte)((i) >>> 24);
        res[1] = (byte)((i) >>> 16);
        res[2] = (byte)((i) >>> 8);
        res[3] = (byte)((i));
        return res;
    }

    /**
     * Convertit 4 bytes en l'entier correspondant
     */
    public static int bytesToInt(byte b[]){
        return     (b[0] & 0xFF) << 24 |
                (b[1] & 0xFF) << 16 |
                (b[2] & 0xFF) << 8 |
                (b[3] & 0xFF);
    }
}
