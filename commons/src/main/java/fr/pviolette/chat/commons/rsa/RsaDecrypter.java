package fr.pviolette.chat.commons.rsa;

import fr.pviolette.chat.commons.Utilities;
import fr.pviolette.chat.commons.rsa.key.RsaPrivateKey;

import java.math.BigInteger;
import java.util.ArrayList;

public class RsaDecrypter {

    private final RsaPrivateKey privateKey;
    private final int blockSize;

    public RsaDecrypter(RsaPrivateKey privateKey, int blockSize) {
        this.privateKey = privateKey;
        this.blockSize = blockSize;
    }

    public byte[] decrypt(byte[] message) {
        byte res[] = null;
        ArrayList<Byte> lst = new ArrayList<Byte>();
        BigInteger bi;
        int i = 0;
        byte newbloc[] = null;
        byte tmpsize[] = new byte[4];
        int size, initsize, newsize;

        //On recupere la taille initiale du message
        tmpsize[0] = message[0];
        tmpsize[1] = message[1];
        tmpsize[2] = message[2];
        tmpsize[3] = message[3];
        initsize = Utilities.bytesToInt(tmpsize);
        i = 4;

        while (i < message.length) {
            tmpsize[0] = message[i];    //On recupere les 4 bytes contenant taille du BigInteger a dechiffrer
            tmpsize[1] = message[i + 1];
            tmpsize[2] = message[i + 2];
            tmpsize[3] = message[i + 3];
            i += 4;
            size = Utilities.bytesToInt(tmpsize);        //On convertit les 4 bytes en un entier
            newbloc = new byte[size];
            for (int j = 0; j < size; j++)
                newbloc[j] = message[i + j];            //On recupere les bytes qui compose le BigInteger à dechiffrer
            bi = new BigInteger(newbloc);            //Ce bloc est convertis en BigInteger
            bi = bi.modPow(this.privateKey.getU(), this.privateKey.getN());            //On applique le chiffrement
            newbloc = bi.toByteArray();        //On recupere les bytes dechiffré
            newsize = newbloc.length;
            for (int j = 0; j < newsize; j++)
                lst.add(newbloc[j]);        //On ajoute les bytes a la liste
            i += size;
        }
        res = new byte[initsize];
        for (i = 0; i < initsize; i++) {    //On convertit la liste en tableau avant de retourner le resultat
            res[i] = lst.get(i);
        }
        return res;
    }

    public RsaPrivateKey getPrivateKey() {
        return privateKey;
    }
}
