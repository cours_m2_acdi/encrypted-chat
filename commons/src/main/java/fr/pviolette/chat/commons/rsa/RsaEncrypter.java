package fr.pviolette.chat.commons.rsa;


import fr.pviolette.chat.commons.Utilities;
import fr.pviolette.chat.commons.rsa.key.RsaPublicKey;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;

public class RsaEncrypter {

    final RsaPublicKey publicKey;
    private int blockSize;

    public RsaEncrypter(RsaPublicKey publicKey, int blockSize) {
        this.publicKey = publicKey;
        this.blockSize = blockSize;
    }

    public byte[] encrypt(byte[] message){
        BigInteger e = this.publicKey.getE();
        BigInteger n = this.publicKey.getN();

        //On utilise un ArrayList car la taille de sortie est différent de la taille d'entrée et n'est pas connue a l'avance
        ArrayList<Byte> lst = new ArrayList<Byte>();
        byte res[] = null;
        BigInteger bi;
        byte size[];
        byte bloc[] = new byte[this.blockSize];
        byte newbloc[] = null;
        int i = 0;

        //On ajoute au debut de la liste la taille du message déchiffré car dans le chiffrement par bloc
        //on complete avec des 0 et de ce fait la taille du messages dechiffré n'est pas forcement la taille d'origine
        size = Utilities.intToBytes(message.length);
        lst.add(0, size[0]);lst.add(1, size[1]);lst.add(2, size[2]);lst.add(3, size[3]);

        while ( i<message.length){
            //On crée un bloc de donnée ainsi une analyse de fréquence ou une recherche exhaustive
            //à partir de la clé publique est inutile de ce fait il est obligatoire de connaitre la clé secrete
            //ou de faire une factorisation de la clé publique pour retrouver la clé privée ce qui est très long
            for (int j = 0; j<this.blockSize;j++){
                if (i+j<message.length){
                    bloc[j] = message[i+j];
                }else{
                    bloc[j] = 0;            //Si on a dépassé le nombre de byte du message alors on complete avec des 0
                }
            }
            bi = new BigInteger(bloc);    //Ce bloc est convertis en BigInteger
            bi = bi.modPow(this.getPublicKey().getE(), this.getPublicKey().getN());            //On applique le chiffrement
            newbloc = bi.toByteArray();            //On convertit en tableau de byte
            size = Utilities.intToBytes(newbloc.length);    //On convertit la taille du tableau en 4 byte
            lst.add(size[0]);lst.add(size[1]);lst.add(size[2]);lst.add(size[3]);    //On ajoute la taille a liste de sortie
            for (int j = 0; j<newbloc.length;j++)
                lst.add(newbloc[j]);        //On ajoute les bytes du chiffré a la liste
            i += blockSize;
        }

        int lstsize = lst.size();
        res = new byte[lstsize];
        for (i=0; i<lstsize; i++)
            res[i] = lst.get(i);        //On met la liste dans un tableau avant de retourner le resultat

        return res;
    }

    public RsaPublicKey getPublicKey() {
        return publicKey;
    }
}
