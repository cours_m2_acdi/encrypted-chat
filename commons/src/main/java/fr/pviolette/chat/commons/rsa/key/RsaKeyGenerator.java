package fr.pviolette.chat.commons.rsa.key;

import java.math.BigInteger;
import java.util.Random;

public class RsaKeyGenerator {

    final private int bitlength;

    public RsaKeyGenerator(int bigLength) {
        this.bitlength = bigLength;
    }


    public RsaKeySet generateKeys(){

        Random r = new Random();

        BigInteger p = BigInteger.probablePrime(bitlength, r);

        BigInteger q = BigInteger.probablePrime(bitlength, r);

        BigInteger N = p.multiply(q);

        BigInteger phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));

        BigInteger e = BigInteger.probablePrime(bitlength / 2, r);

        while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0)

        {

            e.add(BigInteger.ONE);

        }

        BigInteger u = e.modInverse(phi);

        RsaPrivateKey privateKey = new RsaPrivateKey(N, u);
        RsaPublicKey publicKey = new RsaPublicKey(N, e);

        return new RsaKeySet(publicKey, privateKey);
    }
}
