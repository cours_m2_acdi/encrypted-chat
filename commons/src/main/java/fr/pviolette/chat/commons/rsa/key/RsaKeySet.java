package fr.pviolette.chat.commons.rsa.key;

public class RsaKeySet {

    private final RsaPublicKey publicKey;
    private final RsaPrivateKey privateKey;

    public RsaKeySet(RsaPublicKey publicKey, RsaPrivateKey privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public RsaPublicKey getPublicKey() {
        return publicKey;
    }

    public RsaPrivateKey getPrivateKey() {
        return privateKey;
    }

    @Override
    public String toString() {
        return "crypto.rsa.key.RsaKeySet{" +
                "publicKey=" + publicKey +
                ", privateKey=" + privateKey +
                '}';
    }
}
