package fr.pviolette.chat.commons.rsa.key;

import java.math.BigInteger;

public class RsaPrivateKey {

    private final BigInteger n;
    private final BigInteger u;

    public RsaPrivateKey(BigInteger n, BigInteger u) {
        this.n = n;
        this.u = u;
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getU() {
        return u;
    }

    @Override
    public String toString() {
        return "crypto.rsa.key.RsaPrivateKey{" +
                "n=" + n +
                ", u=" + u +
                '}';
    }
}
