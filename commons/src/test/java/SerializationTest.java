import fr.pviolette.chat.commons.Command;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SerializationTest {


    @Test
    void testCommandeJSON(){
        Command command = new Command();
        command.setType(Command.Type.CHANGE_ALIAS);
        command.setArgs(new String[]{"yolo"});

        String jsonStr = command.toJSONString();

        Command command1 = Command.fromJSONString(jsonStr);

        Assertions.assertEquals(command, command1);
    }
}
