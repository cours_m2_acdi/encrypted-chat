package fr.pviolette.chat.server;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

//Un thread pour accepter les connexions.
//Un thread qui écoute et place les messages dans une file d'attente
//Un thread qui vide la file d'attente
public class ChatServer {

    private static Logger logger = LogManager.getLogger();

    public static Options buildOptions() {
        Options options = new Options();

        Option portOption = new Option("p", "port", true, "Server port");
        options.addOption(portOption);

        return options;
    }

    public static void main(String[] args) {

        Options options = ChatServer.buildOptions();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;

        try {

            cmd = parser.parse(options, args);
            Integer port = null;

            if (!cmd.hasOption("port")) {
                logger.info("No port provided - launching server on available port");
            } else {
                try {
                    port = Integer.parseInt(cmd.getOptionValue("port"));
                } catch (NumberFormatException e) {
                    System.out.println("The given port (" + cmd.getOptionValue("port") + ") is not an integer");
                    System.exit(1);
                }
            }

            Controller controller = new Controller();
            if(port == null){
                controller.startServer();
            }
            else{
                controller.startServer(port);
            }

        } catch (IOException e){
            logger.error(e);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("ant", options);
        }
    }
}

