package fr.pviolette.chat.server;

import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;
import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.User;
import fr.pviolette.chat.commons.Utilities;
import fr.pviolette.chat.commons.rsa.RsaEncrypter;
import fr.pviolette.chat.commons.rsa.key.RsaPublicKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.CharBuffer;
import java.util.Optional;

public class ClientProcessor extends Thread{

    private static final Logger logger = LogManager.getLogger();

    private Socket socket;
    private Controller controller;
    private User user;
    private RsaPublicKey publicKey;
    private RsaEncrypter encrypter;

    BufferedInputStream inputStream;

    private static int bufferSize = 256;

    public ClientProcessor(Controller controller, Socket socket,  User user) {
        this.socket = socket;
        this.user = user;
        this.controller = controller;
    }

    @Override
    public void run() {
        logger.debug("Client processor " + this.getClientId() + "reading");
        try{
            while(! socket.isClosed() && socket.isConnected() && !this.isInterrupted()){
                try{
                    this.inputStream = new BufferedInputStream(socket.getInputStream());
                    logger.debug("Waiting for message (id #" + this.user.getId() + ")");
                    byte[] messageBytes = this.read();
                    if(messageBytes.length > 0){
                        logger.debug("Message received : \"" + Utilities.bytesToString(messageBytes) + "\"");

                        String messageJson;
                        byte[] decrypted = this.controller.getDecrypter().decrypt(messageBytes);
                        logger.debug("Decrypted : " + Utilities.bytesToString(decrypted));
                        logger.debug("Decrypting with key " + this.controller.getDecrypter().getPrivateKey());
                        messageJson = new String(decrypted);
                        logger.debug("MessageJson : " + messageJson);

                        if(!messageJson.isEmpty()){
                            Message message = Message.fromJsonString(messageJson);
                            message.setId(this.controller.getNewMessageId());
                            message.setSender(this.user);

                            if(message.getRecipient() != null){
                                Optional<User> optional = controller.getByAlias(message.getRecipient().getAlias());

                                optional.ifPresent(message::setRecipient);
                            }

                            this.controller.getMessageHandler().queueMessage(message);
                        }
                    }
                }catch (IOException e ){
                    logger.error("Error while reading", e);
                }catch (JsonSyntaxException e){
                    logger.error("Invalid message", e);
                }
            }
        } catch (ClientDisconnectedException e){
            this.controller.clientDisconnected(this);
        }
    }

    void initialize() throws IOException{
        this.inputStream = new BufferedInputStream(socket.getInputStream());
        logger.debug("Client processor " + this.getClientId() + " initialized");
    }

    public int getClientId() {
        return user.getId();
    }

    //La méthode que nous utilisons pour lire les réponses
    private byte[] read() throws IOException, ClientDisconnectedException {

        byte[] bytes = new byte[bufferSize];
        int length = inputStream.read(bytes);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        logger.debug("Length : " + length);
        if(length == -1){
            throw new ClientDisconnectedException();
        }
        byteArrayOutputStream.write(bytes, 0, length);
//        StringBuilder stringBuilder = new StringBuilder(new String(bytes, 0 , lenght));
        while(inputStream.available() != 0){
            length = inputStream.read(bytes);
            byteArrayOutputStream.write(bytes, 0, length);
        }
        return byteArrayOutputStream.toByteArray();
    }

    public InetAddress getInetAdress(){
        return socket.getInetAddress();
    }

    private synchronized void write(String message) throws IOException{
        if(!socket.isClosed()){
            BufferedOutputStream outputStream = new BufferedOutputStream(socket.getOutputStream());
            logger.debug("Writing : " + message);

            byte[] messageBytes;
            if(this.encrypter == null) {
                logger.debug("Sending non encrypted message");
                messageBytes = message.getBytes();
            }else{
                logger.debug("Encrypting message");
                messageBytes = this.encrypter.encrypt(message.getBytes());
                logger.debug("Encrypted : \"" + Utilities.bytesToString(messageBytes));
            }
            outputStream.write(messageBytes);
            outputStream.flush();
        }
    }

    public void sendMessage(Message message) throws IOException{
        this.write(message.toJsonString());
    }

    public String getAlias() {
        return user.getAlias();
    }

    public void setAlias(String alias){
        this.setAlias(alias, true);
    }

    public void setAlias(String alias, boolean sendNotifMessage) {
        if(! alias.equals(this.getAlias())){

            String oldAlias = this.getAlias();

            this.user.setAlias(alias);

            if(sendNotifMessage){
                //Send the update to all user;

                Message message = this.controller.getMessageFactory().getAliasUpdateMessage(oldAlias, this.user);
                message.setSender(this.user);

                this.controller.getMessageHandler().queueMessage(message);
            }
        }
    }

    public User getUser() {
        return user;
    }

    public RsaPublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(RsaPublicKey publicKey) {
        logger.debug("Client public key : " + publicKey);
        this.publicKey = publicKey;
        BigInteger bi = publicKey.getE().modPow(publicKey.getE(), publicKey.getN());
        //On aura donc des blocs toujours inferieur à n
        int blocsize = bi.toByteArray().length - 10;

        this.encrypter = new RsaEncrypter(this.publicKey, blocsize);
    }
}
