package fr.pviolette.chat.server;

import fr.pviolette.chat.commons.Command;
import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.User;
import fr.pviolette.chat.commons.rsa.key.RsaPublicKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;

public class CommandHandler {

    private static Logger logger = LogManager.getLogger();

    Controller controller;

    public CommandHandler(Controller controller) {
        this.controller = controller;
    }

    public void handleCommande(Message message){
        if(message.getType() != Message.Type.COMMANDE_PRIVATE && message.getType() != Message.Type.COMMAND_ALL){
            throw new IllegalArgumentException("Not a command !");
        }

        logger.debug("JSONString -> " + message.getContent());
        Command command = Command.fromJSONString(message.getContent());
        logger.debug(command);

        switch (command.getType()){
            case CHANGE_ALIAS:
                handleChangeAlias(message.getSender(), command);
                break;
            case LIST_USER:
                handleListUserCommand(message.getSender());
                break;
            case PUBLIC_KEY:
                handlePublicKeyMessage(message.getSender(), command);
                break;
            default:
                logger.info("Unknown command type " + command.toString());
        }
    }

    private void handlePublicKeyMessage(User sender, Command command){

        String[] args = command.getArgs();

        if(args == null || args.length < 2){
            logger.error("Not a valid PublicKey command" + command.toString());
            return;
        }

        ClientProcessor clientProcessor = this.controller.getClientProcessor(sender.getId());
//        Should we allow user to redefine its public key ?

//        if(clientProcessor.getPublicKey() != null){
//            logger.error("User already have a public key - " + command.toString());
//            return;
//        }

        try{
            BigInteger n = new BigInteger(args[0]);
            BigInteger e = new BigInteger(args[1]);

            RsaPublicKey publicKey = new RsaPublicKey(n, e);

            clientProcessor.setPublicKey(publicKey);
            logger.debug("Client public key : " + publicKey.toString());

            if(args.length > 2){
                StringBuilder alias = new StringBuilder();
                for(int i = 2; i < args.length; ++i){
                    alias.append(args[i]).append(" ");
                }
                this.setClientAlias(clientProcessor.getUser(), alias.toString(), false);
            }

            Message message = this.controller.getMessageFactory().newConnectionMessage(clientProcessor.getUser());
            this.controller.getMessageHandler().queueMessage(message);
        } catch (NumberFormatException e){
            logger.error("Not a valid public key command : " + command.toString());
            //TODO give error to client
        }
    }


    private void handleListUserCommand(User sender) {

        Message message = controller.getMessageFactory().getListUserMessage();

        message.setRecipient(sender);

        controller.getMessageHandler().queueMessage(message);

    }

    private void handleChangeAlias(User user, Command command){

        String args[] = command.getArgs();
        if(args == null || args.length == 0){
            logger.error("Not a valide ChangeAlias command : " + command.toString());
            return;
        }

        String newAlias = args[0];

        this.setClientAlias(user, newAlias, true);
        //Check that new alias is not the same as before
    }

    protected void setClientAlias(User user, String newAlias, boolean sendNotifMessage){

        newAlias = newAlias.trim();
        if(user.getAlias().equals(newAlias)){
            logger.info("Same alias as before");
            return;
        }

        //Check if newAlias is available

        String suffix = "";
        int suffixNumber = 0;

        ClientProcessor senderProcessor = this.controller.getClientProcessor(user.getId());

        if(senderProcessor == null){
            logger.error("Can't find client processor for user " + user.getId());
            return;
        }

        boolean available = false;

        while(!available){
            available = true;
            for(ClientProcessor clientProcessor : controller.getClientProcessors()){
                if(clientProcessor.getAlias().equals(newAlias + suffix )){
                    ++suffixNumber;
                    suffix = String.valueOf(suffixNumber);
                    available = false;
                }
            }
        }
        newAlias = newAlias + suffix;
        senderProcessor.setAlias(newAlias, sendNotifMessage);
    }


}
