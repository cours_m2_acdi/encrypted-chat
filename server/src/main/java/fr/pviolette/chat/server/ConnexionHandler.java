package fr.pviolette.chat.server;

import fr.pviolette.chat.commons.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Vector;

public class ConnexionHandler extends Thread {

    private static Logger logger = LogManager.getLogger();

    ServerSocket serverSocket;

    int port;

    private Controller controller;

    public ConnexionHandler(Controller controller){
        this.controller = controller;
    }

    public void openServerSocket(int port) throws IOException{
        this.serverSocket = new ServerSocket(port);
        logger.info("ServerSocket opened on port " + this.serverSocket.getLocalPort() );
    }

    public void openServerSocket() throws IOException{
        this.serverSocket = new ServerSocket();
        logger.info("ServerSocket opened on port " + this.serverSocket.getLocalPort() );

    }

    @Override
    public void run() {
        logger.info("Starting ConnexionHandler thread");
        try {
            while (!this.isInterrupted()) {
                logger.info("Waiting for connexion on port " + this.serverSocket.getLocalPort());
                Socket newClientSocket = serverSocket.accept();

                logger.info("New client from " + newClientSocket.getInetAddress() + ":" + newClientSocket.getPort());

                controller.addUser(newClientSocket);

            }
        }catch (IOException e){
            logger.error("Error waiting for connexion", e);
        }
    }
}
