package fr.pviolette.chat.server;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.User;
import fr.pviolette.chat.commons.Utilities;
import fr.pviolette.chat.commons.rsa.RsaDecrypter;
import fr.pviolette.chat.commons.rsa.RsaEncrypter;
import fr.pviolette.chat.commons.rsa.key.RsaKeyGenerator;
import fr.pviolette.chat.commons.rsa.key.RsaKeySet;
import fr.pviolette.chat.commons.rsa.key.RsaPublicKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Controller {

    private static final Logger logger = LogManager.getLogger();

    private int lastId;
    private int lastMessageId;

    private Map<Integer, ClientProcessor> clientProcessorMap;

    private MessageHandler messageHandler;
    private ConnexionHandler connexionHandler;
    private CommandHandler commandHandler;
    private MessageFactory messageFactory;

    private RsaKeySet keySet;
    private RsaDecrypter decrypter;

    public Controller() {
        this.lastId = 0;
        this.lastMessageId = 0;
        this.clientProcessorMap = Maps.newConcurrentMap();
        this.connexionHandler = new ConnexionHandler(this);
        this.messageHandler = new MessageHandler(this, connexionHandler);
        this.commandHandler = new CommandHandler(this);
        this.messageFactory = new MessageFactory(this);
    }

    private void initRsaKeys(){
        logger.debug("Generating keys...");
        RsaKeyGenerator generator = new RsaKeyGenerator(128);

        logger.debug("Key generated");
        this.keySet = generator.generateKeys();

        //On applique le chiffrement une fois pour avoir une idée du nombre de byte que l'on peut mettre par bloc
        //Ce nombre doit etre inferieur a n sinon l'operation de chiffrement et de dechiffrement n'est plus valable
        BigInteger bi = this.keySet.getPublicKey().getE().modPow(this.keySet.getPublicKey().getE(), this.keySet.getPublicKey().getN());
        //On aura donc des blocs toujours inferieur à n
        int blocsize = bi.toByteArray().length - 10;


        this.decrypter = new RsaDecrypter(this.keySet.getPrivateKey(), blocsize);

        logger.debug("Public key : " + this.keySet.getPublicKey());
        logger.debug("Private key : " + this.keySet.getPrivateKey());

        //Test
        Message message = this.messageFactory.getListUserMessage();

        logger.debug(message.toJsonString());
        byte[] msg = (message.toJsonString()).getBytes();
            byte[] encrypted = (new RsaEncrypter(this.keySet.getPublicKey(), blocsize)).encrypt(msg);
            logger.debug("Encrypted : " + Utilities.bytesToString(encrypted));
            logger.debug("Decrypt test ; " + new String(this.decrypter.decrypt(encrypted)));
    }

    public void startServer(int port) throws IOException{

        this.initRsaKeys();

        this.connexionHandler.openServerSocket(port);

        this.connexionHandler.start();
        this.messageHandler.start();
    }

    public void startServer() throws IOException{

        this.initRsaKeys();

        this.connexionHandler.openServerSocket(0);

        this.connexionHandler.start();
        this.messageHandler.start();
    }

    public void addUser(Socket socket){
        User user = new User(++lastId);
        ClientProcessor clientProcessor = new ClientProcessor(this, socket, user);
        try{
            clientProcessor.initialize();

            clientProcessor.start();

            clientProcessorMap.put(user.getId(), clientProcessor);

//          Send public key to the client
            Message message = this.messageFactory.getPublicKeyMessage(user, this.keySet.getPublicKey());
            clientProcessor.sendMessage(message);

//            User is not considered as connected as long as the user has not given its key
//            Message newConnectionMessage = this.messageFactory.newConnectionMessage(user);

//            this.messageHandler.queueMessage(newConnectionMessage);

        }catch (IOException e){
            logger.error("Unable to initialize clientProcessor " + clientProcessor.getClientId());
            try{
                socket.close();
            }catch (IOException e2){
                logger.error("Wtf", e2);
            }
        }
    }

    MessageHandler getMessageHandler(){
        return this.messageHandler;
    }

    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    ClientProcessor getClientProcessor(int id){
        return this.clientProcessorMap.get(id);
    }

    Collection<ClientProcessor> getClientProcessors(){
        return ImmutableMap.copyOf(clientProcessorMap).values();
    }

    synchronized int getNewMessageId(){
        return ++lastMessageId;
    }

    public MessageFactory getMessageFactory() {
        return messageFactory;
    }


    Optional<User> getByAlias(String alias) {

        for(ClientProcessor clientProcessor : this.clientProcessorMap.values()){
            if(clientProcessor.getAlias().equals(alias)){
                return Optional.of(clientProcessor.getUser());
            }
        }
        return Optional.empty();
    }

    public RsaPublicKey getPublicKey(){
        return this.keySet.getPublicKey();
    }

    public RsaDecrypter getDecrypter() {
        return decrypter;
    }

    public void clientDisconnected(ClientProcessor clientProcessor) {
        Message disconnectMessage = this.messageFactory.getDisconnectionMessage(clientProcessor.getUser());
        this.messageHandler.queueMessage(disconnectMessage);

        this.clientProcessorMap.remove(clientProcessor.getClientId());

        try {
            clientProcessor.join();
            logger.debug("Disconnection of client " + clientProcessor.getUser() + " ok");
        } catch (InterruptedException e) {
            logger.error("Disconnect error ??? ", e);
        }
    }
}
