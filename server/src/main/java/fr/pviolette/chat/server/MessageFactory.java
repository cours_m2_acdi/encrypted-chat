package fr.pviolette.chat.server;

import fr.pviolette.chat.commons.Command;
import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.User;
import fr.pviolette.chat.commons.rsa.key.RsaPublicKey;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class MessageFactory {

    private Controller controller;

    public MessageFactory(Controller controller) {
        this.controller = controller;
    }

    public Message getAliasUpdateMessage(String oldAlias, User user){

        Message message = new Message(Instant.now(), oldAlias + " is know known as " + user.getAlias(), Message.Type.SERVER_ALL);
        message.setSender(user);
        message.setId(controller.getNewMessageId());

        return message;
    }

    public Message getDisconnectionMessage(User user){
        Message message = new Message(Instant.now(), "", Message.Type.USER_DISCONNECTION);
        message.setId(controller.getNewMessageId());
        message.setSender(user);

        return message;
    }

    public Message newConnectionMessage(User user){
        Message message = new Message(Instant.now(), "", Message.Type.USER_CONNECTION);
        message.setId(controller.getNewMessageId());
        message.setSender(user);

        return message;
    }

    public Message getPublicKeyMessage(User recipient, RsaPublicKey key){
        Command command = new Command();
        command.setType(Command.Type.PUBLIC_KEY);
        command.setArgs(new String[]{key.getN().toString(), key.getE().toString()});

        Message message = new Message(Instant.now(), command.toJSONString(), Message.Type.COMMANDE_PRIVATE);
        message.setRecipient(recipient);

        return message;
    }

    public Message getListUserMessage(){
        Collection<ClientProcessor> clientProcessors = controller.getClientProcessors();
        StringBuilder contentBuilder =
                new StringBuilder()
                        .append(clientProcessors.size())
                        .append(" client");
        if(clientProcessors.size() > 1){
            contentBuilder.append("s");
        }
        contentBuilder.append(" connected ------");
        for(ClientProcessor clientProcessor : clientProcessors){
            contentBuilder.append("\n\t" + clientProcessor.getAlias());
        }

        return new Message(Instant.now(), contentBuilder.toString(), Message.Type.SERVER_PRIVATE);
    }
}
