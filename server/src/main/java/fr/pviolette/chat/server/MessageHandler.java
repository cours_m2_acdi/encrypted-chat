package fr.pviolette.chat.server;

import fr.pviolette.chat.commons.Message;
import fr.pviolette.chat.commons.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.Instant;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MessageHandler extends Thread {

    private static Logger logger;
    private ConnexionHandler handler;
    private Queue<Message> messageQueue;
    private Controller controller;

    public MessageHandler(Controller controller, ConnexionHandler handler) {
        this.handler = handler;
        this.controller = controller;
        messageQueue = new ConcurrentLinkedQueue<>();
    }

    private Logger getLogger() {
        if (logger == null) {
            logger = LogManager.getLogger();
        }
        return logger;
    }

    public void queueMessage(Message message) {
        messageQueue.add(message);
        getLogger().debug("Message " + message.getId() + " queued");
    }

    @Override
    public void run() {
        try {
            while (!this.isInterrupted()) {
                while (!messageQueue.isEmpty()) {

                    Message msg = messageQueue.peek();
                    messageQueue.remove();

                    logger.debug("Handling message " + msg.getId());

                    if (msg.getType() == Message.Type.COMMANDE_PRIVATE
                            || msg.getType() == Message.Type.COMMAND_ALL) {
                        //Let the command handler handle it
                        logger.debug("Message is command");
                        this.controller.getCommandHandler().handleCommande(msg);

                    } else if (msg.getType().isSendToAll()) {
                        //We send the message to every connected user
                        sendAll(msg);

                    } else {

                        //We send the message only to the recipient

                        User receiver = msg.getRecipient();

                        if (receiver == null) {
                            logger.error("Message is private must has not dest");
                        } else {
                            ClientProcessor receiverProcessor = controller.getClientProcessor(receiver.getId());

                            if (receiverProcessor == null) {
                                logger.error("Can't retrieve clientProcessor for user (" + receiver.getId() + ") " + receiver.getAlias());
                                Message errorMsg =
                                        new Message(Instant.now(), "Can't find user " + receiver.getAlias(), Message.Type.SERVER_PRIVATE);
                                errorMsg.setRecipient(msg.getSender());
                                this.queueMessage(errorMsg);
                            } else {
                                sendTo(msg, receiverProcessor);
                            }
                        }
                    }
                }
                sleep(100); //Pas beau
            }
        } catch (InterruptedException e) {

        }
    }

    private void sendTo(Message msg, ClientProcessor receiver) {
        try {
            receiver.sendMessage(msg);
        } catch (IOException e) {
            logger.error("Unable to send message " + msg.getId() + " to " + receiver.getInetAdress(), e.getMessage());
        }
    }

    public void sendAll(Message msg) {
        //Send message to everyone
        for (ClientProcessor cp : controller.getClientProcessors()) {
            sendTo(msg, cp);
        }
    }
}
