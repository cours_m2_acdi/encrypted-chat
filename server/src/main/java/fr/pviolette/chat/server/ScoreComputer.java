package fr.pviolette.chat.server;

import java.util.Arrays;

public class ScoreComputer {

    int computeScore(int[] dices){

        if(checkFor55(dices)){
            return 5000;
        }else if(checkForSuite(dices)){
            return 1500;
        }else{
            int brelan = checkBrelan(dices);
            int plusScore = 0;
            int occ1 = 0;
            int occ5 = 0;
            for(int dice : dices){
               if(dice == 1){
                   occ1++;
               }
               if(dice == 5){
                   occ5++;
               }
            }
            if(brelan == 1){
                occ1 -= 3;
            }
            if(brelan == 5){
                occ5 -= 3;
            }
            return occ5 * 50 + occ1 * 100 + (brelan == 1 ? 1000 : brelan * 100);
        }

    }

    private boolean checkFor55(int[] dices){
        return Arrays.equals(dices, new int[]{5,5,5,5,5});
    }

    private boolean checkForSuite(int[] dices){
        return Arrays.equals(dices, new int[]{2,3,4,5,6})
                ||Arrays.equals(dices, new int[]{1,2,3,4,5});
    }

    private int checkBrelan(int [] dices){
        if(dices.length < 3){
            return 0;
        }

        int[] occ = new int[]{0,0,0,0,0,0};

        for(int dice : dices){
            occ[dice - 1]++;
            if(occ[dice - 1] == 3){
                return dice;
            }
        }
        return 0;
    }

}
